#pragma once
#include <iostream>

using namespace std;
#include "boss.h"
#include "employee.h"
#include "manager.h"
#include <fstream>
const string FILENAME = "SaveEmploy.txt";
class workerManager
{
public:
	//构造函数
	workerManager();
	//打印菜单
	void showMenu();
	//退出系统
	void exitSystem();
	//添加职工
	void addEmployee();
	//析构函数
	~workerManager();

	//保存文件
	void saveFile();
	//计算文件中的有效信息数,
	int getNum();
	//导入信息，完成初始化
	void initEmployee();
	//打印输出数据
	void showEmployee();
	//判断职工是否存在,存在，返回所在数据下标，不存在，返回-1
	int isExit(int id);
	//删除职工
	void delEmployee();
	//修改职工信息
	void modefyEmployee();
	//查找职工
	void searchEmployee();
	//用选择排序的思想对所以员工进行排序
	void sortEmployee();
	//清空文档
	void cleanFile();
	bool isEmpty;  // 记录文件是否为空

	work** workStart;   //指向存储的起始地址
	int curSize;  //当前存的员工信息数
	int capacity;  //当前容量
};

