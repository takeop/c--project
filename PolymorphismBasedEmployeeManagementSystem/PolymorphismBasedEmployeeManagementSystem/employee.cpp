#define _CRT_SECURE_NO_WARNINGS 1

#include "employee.h"

//显示员工信息
void employee::showInfo()
{
	cout << "员工id  " << this->m_Id
		<< "\t员工姓名:  " << this->m_name
		<< "\t员工岗位:  " << this->getDepName()
		<< "\t岗位职责:  " << this->showDuty() << endl;
}
//获取岗位名称
string employee::getDepName()
{
	return (string)("员工");
}
employee::employee(int id, string name, int depId = 1)
{
	this->m_Id = id;
	this->m_name = name;
	this->m_depId = depId;
}

string employee:: showDuty()
{
	return (string)("完成经理的任务");
}