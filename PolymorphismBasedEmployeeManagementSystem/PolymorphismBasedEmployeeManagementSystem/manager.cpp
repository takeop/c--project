#define _CRT_SECURE_NO_WARNINGS 1
#include "manager.h"


void manager::showInfo()
{
	cout << "员工id  " << this->m_Id
		<< "\t员工姓名:  " << this->m_name
		<< "\t员工岗位:  " << this->getDepName()
		<< "\t岗位职责:  " << this->showDuty() << endl;
}

string manager::getDepName()
{
	return (string)("经理");
}

string manager::showDuty()
{
	return (string)("完成老板的任务,下发任务给员工");
}

manager::manager(int id, string name, int depId)
{
	this->m_Id = id;
	this->m_name = name;
	this->m_depId = depId;
}