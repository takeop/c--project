#pragma once
#include <iostream>
using namespace std;
#include <string>

//职工抽象基类
class work
{
public:
	//显示员工信息
	virtual void showInfo() = 0;
	//获取岗位名称
	virtual string getDepName() = 0;
	//岗位职责
	virtual string showDuty() = 0;

	int m_Id;
	string m_name;
	int m_depId;
};