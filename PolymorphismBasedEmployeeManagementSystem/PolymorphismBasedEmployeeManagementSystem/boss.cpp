#define _CRT_SECURE_NO_WARNINGS 1
#include "boss.h"


//构造函数初始化
boss::boss(int id, string name, int depId = 3)
{
	this->m_Id = id;
	this->m_name = name;
	this->m_depId = depId;
}
//显示员工信息
void boss::showInfo()
{
	cout << "员工id  " << this->m_Id
		<< "\t员工姓名:  " << this->m_name
		<< "\t员工岗位:  " << this->getDepName()
		<< "\t岗位职责:  " << this->showDuty() << endl;
}
//获取岗位名称
string boss::getDepName()
{
	return (string)("老板");
}
//岗位职责
string boss::showDuty()
{
	return (string)("安排公司的计划，发放任务给经理");
}