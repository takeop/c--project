#include "workerManager.h"

int workerManager::getNum()
{
	ifstream ifs;
	ifs.open(FILENAME, ios::in);
	if (!ifs.is_open())
	{
		cout << "打开失败" << endl;
		exit(-1);
	}
	int num = 0;
	int id = 0;
	string name = " ";
	int depId = 0;
	while (ifs >> id && ifs >> name && ifs >> depId)
	{
		//记录人数
		num++;
	}
	ifs.close();
	return num;
}
workerManager::workerManager()
{
	//打开文件
	ifstream ifs;
	ifs.open(FILENAME, ios::in);

	//文件不存在
	if (!ifs.is_open())
	{
		cout << "文件不存在" << endl;
		this->workStart = nullptr;
		this->capacity = 0;
		this->curSize = 0;
		this->isEmpty = true;
		//关闭文件
		ifs.close();
		return;
	}
	//文件为空
	char ch;
	ifs >> ch;
	if (ifs.eof())
	{
		cout << "文件为空" << endl;
		this->workStart = nullptr;
		this->capacity = 0;
		this->curSize = 0;
		this->isEmpty = true;
		//关闭文件
		ifs.close();
		return;
	}
	//文件不为空
	this->isEmpty = false;
	int num = this->getNum();
	cout << "导入" << num << "个职工信息" << endl;
	//初始化，导入信息
	initEmployee();
}
int workerManager::isExit(int id)
{
	int i = 0;
	int ret = -1;
	for (i = 0; i < this->curSize; i++)
	{
		if (id == this->workStart[i]->m_Id)
		{
			ret = i;
			break;
		}
	}
	return ret;
}

void workerManager::modefyEmployee()
{
	//判断文件是否存在
	if (this->isEmpty)
	{
		cout << "文件为空" << endl;
	}
	else
	{
		cout << "请输入修改职工id" << endl;
		int id = 0;
		cin >> id;
		int ret = this->isExit(id);
		int i = 0;
		if (ret == -1)
		{
			cout << "职工不存在" << endl;
		}
		else
		{
			cout << "职工编号为" << id << endl;
			//修改职工
				//先删除旧的职工，再重新申请空间，赋值新职工
			//ret是数组下标，id是当前职工的id
			delete this->workStart[ret];
			this->workStart[ret] = nullptr;
			int newId = 0;
			string newName = " ";
			int newDepId = 0;

			cout << "请输入修改后员工的id" << endl;
			cin >> newId;
			cout << "请输入修改后员工的姓名" << endl;
			cin >> newName;

			int flag = 1;
			work* work = nullptr;
			while (flag)
			{
				cout << "请选择员工岗位" << endl;
				cout << " 1.普通员工 " << endl;
				cout << " 2.经理 " << endl;
				cout << " 3.老板 " << endl;
				cin >> newDepId;
				switch (newDepId)
				{
				case 0:
					cout << "退出输入" << endl;
					flag = 0;
					break;
				case 1:
					work = new employee(newId, newName, newDepId);
					flag = 0;
					break;
				case 2:
					work = new manager(newId, newName, newDepId);
					flag = 0;
					break;
				case 3:
					work = new boss(newId, newName, newDepId);
					flag = 0;
					break;
				default:
					cout << "输入有误,请重新输入" << endl;
					break;
				}
			}
			if (work == nullptr)
			{
				cout << "申请失败" << endl;
				exit(-1);
			}
			//保存信息到数组中
			this->workStart[ret] = work;

			cout << "修改成功" << endl;
			//自动保存文件
			this->saveFile();
		}
	}

	system("pause");
	system("cls");
}
void workerManager::cleanFile()
{
	//清空文档
	cout << "是否确认清空文件" << endl;
	cout << "1.确认清空" << endl;
	cout << "2.取消清空" << endl;
	int select = 0;
	cin >> select;
	if (select == 1)
	{
		//打开模式ios::trunc打开文件，如果文件存在，则删除并重新创建
		ofstream ofs(FILENAME, ios::trunc);
		ofs.close();

		//清空释放内存
		if (this->workStart != nullptr)
		{
			//释放存储数据的内存，再释放数组内存，防止内存泄漏
			for (int i = 0; i < this->curSize; i++)
			{
				if (this->workStart[i] != nullptr)
				{
					delete this->workStart[i];
					this->workStart[i] = nullptr;
				}
			}
			this->curSize = 0;

			delete[]this->workStart;
			this->workStart = nullptr;

			this->capacity = 0;
			this->isEmpty = true;
		}
		cout << "清空成功" << endl;
	}
	else
	{
		cout << "取消清空，退出功能" << endl;
	}

	system("pause");
	system("cls");
}
void workerManager::searchEmployee()
{
	//判断文件是否存在数据
	if (this->isEmpty)
	{
		cout << "文件为空" << endl;
	}
	else
	{
		bool flag = true;
		while (flag)
		{
			//选择按职工id查找还是职工name查找
			cout << "请输入查找方式" << endl;
			cout << "1.通过 id 查找" << endl;
			cout << "2.通过 姓名 查找" << endl;
			int select = 0;
			cin >> select;

			//通过 id 查找
			if (select == 1)
			{
				int id = 0;
				cout << "请输入查找职工的id" << endl;
				cin >> id;
				//接受数组的下标
				int ret = this->isExit(id);
				if (ret == -1)
				{
					cout << "查无此人" << endl;
				}
				else
				{
					cout << "找到职工，职工的信息如下" << endl;
					this->workStart[ret]->showInfo();
				}
				flag = false;
			}//通过 姓名 查找
			else if (select == 2)
			{
				string name = " ";
				int fg = 0;
				cout << "请输入查找的员工姓名" << endl;
				cin >> name;
				for (int i = 0; i < this->curSize; i++)
				{
					if (this->workStart[i]->m_name == name)
					{
						cout << "查找成功" << endl;
						fg = 1;
						cout << "找到职工，职工的信息如下" << endl;
						this->workStart[i]->showInfo();
						break;
					}
				}
				if (fg == 0)
				{
					cout << "查找失败，查无此人" << endl;
				}
				flag = false;
			}
			else
			{
				cout << "输入有误，无此选项" << endl;
				cout << "请重新输入" << endl;
			}
		}
	}
	system("pause");
	system("cls");
}
void workerManager::sortEmployee()
{
	//判断文件是否存在
	if (this->isEmpty)
	{
		cout << "文件为空" << endl;
		system("pause");
		system("cls");
	}
	else
	{
		bool flag = true;
		while (flag)
		{
			cout << "请选择排序方式" << endl;
			cout << "1. 升序排列" << endl;
			cout << "2. 降序排列" << endl;
			int select = 0;
			cin >> select;
			if (select == 1)
			{
				//升序
				int i = 0;
				for (i = 0; i < this->curSize; i++)
				{
					int minOrMax = i;
					for (int j = i + 1; j < this->curSize; j++)
					{
						if (this->workStart[j]->m_Id < this->workStart[minOrMax]->m_Id)
						{
							minOrMax = j;
						}
					}
					if (i != minOrMax)
					{
						work* tmp = this->workStart[i];
						this->workStart[i] = this->workStart[minOrMax];
						this->workStart[minOrMax] = tmp;
					}
				}
				cout << "排序成功,排序结果为" << endl;
				this->showEmployee();
				//自动保存
				this->saveFile();
				flag = false;
			}//降序
			else if (select == 2)
			{
				int i = 0;
				for (i = 0; i < this->curSize; i++)
				{
					int minOrMax = i;
					for (int j = i + 1; j < this->curSize; j++)
					{
						if (this->workStart[j]->m_Id > this->workStart[minOrMax]->m_Id)
						{
							minOrMax = j;
						}
					}
					if (i != minOrMax)
					{
						work* tmp = this->workStart[i];
						this->workStart[i] = this->workStart[minOrMax];
						this->workStart[minOrMax] = tmp;
					}
				}
				cout << "排序成功,排序结果为" << endl;
				this->showEmployee();
				//自动保存
				this->saveFile();
				flag = false;
			}
			else
			{
				cout << "输入有误，无此选项" << endl;
				cout << "请重新输入" << endl;
			}
		}
	}
}
void workerManager::delEmployee()
{
	//判断文件是否存在
	if (this->isEmpty)
	{
		cout << "文件为空" << endl;
	}
	else
	{
		cout << "请输入删除职工id" << endl;
		int id = 0;
		cin >> id;
		int ret = this->isExit(id);
		int i = 0;
		if (ret == -1)
		{
			cout << "职工不存在" << endl;
		}
		else
		{
			cout << "职工编号为" << ret << endl;
			for (i = ret; i < this->curSize - 1; i++)
			{
				this->workStart[i] = this->workStart[i + 1];
			}
			this->curSize--;
			cout << "删除成功" << endl;
			//自动保存文件
			this->saveFile();
		}
	}
	//按任意键存在，清屏
	system("pause");
	system("cls");
}
void workerManager::initEmployee()
{
	ifstream ifs;
	ifs.open(FILENAME, ios::in);

	int id = 0;
	string name = " ";
	int depId = 0;
	//申请空间
	int num = this->getNum();
	work** newWork = new work * [num];
	if (newWork == nullptr)
	{
		cout << " workerManager::initEmployee申请失败" << endl;
		exit(-1);
	}
	this->capacity = num;
	//更新地址
	this->workStart = newWork;
	//根据部门确定职工类型
	work* worker = nullptr;
	while (ifs >> id && ifs >> name && ifs >> depId)
	{
		//根据部门确定职工类型
		switch (depId)
		{
		case 1:
			worker = new employee(id, name, depId);
			break;
		case 2:
			worker = new manager(id, name, depId);
			break;
		case 3:
			worker = new boss(id, name, depId);
			break;
		default:
			cout << "输入错误" << endl;
			break;
		}
		if (worker == nullptr)
		{
			cout << " workerManager::initEmployee申请失败" << endl;
			exit(-1);
		}
		this->workStart[this->curSize] = worker;

		this->curSize++;
	}

	ifs.close();
}

void workerManager::showEmployee()
{
	if (isEmpty)
	{
		cout << "文件为空" << endl;
	}
	else
	{
		int i = 0;
		for (i = 0; i < this->curSize; i++)
		{
			//调用多态函数实现打印
			this->workStart[i]->showInfo();
		}
	}
	system("pause");
	system("cls");
}
void workerManager::saveFile()
{
	ofstream ofs;
	ofs.open(FILENAME, ios::out);
	if (!ofs.is_open())
	{
		cout << "打开失败" << endl;
		exit(-1);
	}
	int i = 0;
	for (i = 0; i < this->curSize; i++)
	{
		ofs << this->workStart[i]->m_Id << " "
			<< this->workStart[i]->m_name << " "
			<< this->workStart[i]->m_depId << endl;
	}
	ofs.close();
}
workerManager::~workerManager()
{
	if (this->workStart != nullptr)
	{
		for (int i = 0; i < this->curSize; i++)
		{
			if (this->workStart[i] != nullptr)
			{
				delete this->workStart[i];
				this->workStart[i] = nullptr;
			}
		}
		delete[] this->workStart;
		this->workStart = nullptr;
	}
}

void workerManager::showMenu()
{
	cout << "*******************************************" << endl;
	cout << "********** 欢迎使用职工管理系统  ************" << endl;
	cout << "************ 0.退出管理程序 ****************" << endl;
	cout << "************ 1.增加职工信息 ****************" << endl;
	cout << "************ 2.显示职工信息 ****************" << endl;
	cout << "************ 3.删除离职员工 ****************" << endl;
	cout << "************ 4.修改职工信息 ****************" << endl;
	cout << "************ 5.查找职工信息 ****************" << endl;
	cout << "************ 6.按照编号排序 ****************" << endl;
	cout << "************ 7.清空所有文档 ****************" << endl;
	cout << "*******************************************" << endl;
	cout << endl;
}

void workerManager::exitSystem()
{
	//保存内容到文件中
	this->saveFile();
	this->isEmpty = false;
	cout << "保存完毕" << endl;
	cout << "退出成功，欢迎下次使用" << endl;
	system("pause");
	exit(0);
}

void workerManager::addEmployee()
{
	//提示本次添加存储多少人
	cout << "请输入添加人数：";
	int addNum = 0;
	cin >> addNum;

	int newCapacity = 0;
	work** newWorkStart = nullptr;
	//检查输入的合法性
	if (addNum > 0 && addNum <= 10)
	{
		//检查增容
		if ((addNum + this->curSize) > this->capacity)
		{
			newCapacity = this->capacity == 0 ? 10 : 2 * this->capacity;
			newWorkStart = new work * [newCapacity];
			if (newWorkStart == nullptr)
			{
				cout << "申请失败" << endl;
				exit(-1);
			}
			//更新容量
			this->capacity = newCapacity;
		}
		//申请成功后，将旧的数据copy到新的数据中
		if (this->workStart != nullptr)
		{
			int i = 0;
			for (i = 0; i < this->curSize; i++)
			{
				if (newWorkStart == nullptr)
				{
					cout << "newWorkStart == nullptr" << endl;
					exit(-1);
				}
				newWorkStart[i] = this->workStart[i];
			}
			delete[] this->workStart;
			this->workStart = nullptr;
		}

		//更新存储地址
		this->workStart = newWorkStart;

		//添加新数据
		int i = 0;
		for (i = 0; i < addNum; i++)
		{
			int id = 0;   //职工编号
			string name = " ";  //职工姓名

			int depId = 0;  //职工部门选择

			cout << "请输入第" << i + 1 << "个新员工的编号" << endl;
			cin >> id;

			cout << "请输入第" << i + 1 << "个新员工的姓名" << endl;
			cin >> name;

			int flag = 1;
			work* work = nullptr;
			while (flag)
			{
				cout << "请选择员工岗位" << endl;
				cout << " 1.普通员工 " << endl;
				cout << " 2.经理 " << endl;
				cout << " 3.老板 " << endl;
				cin >> depId;
				switch (depId)
				{
				case 0:
					cout << "退出输入" << endl;
					flag = 0;
					break;
				case 1:
					work = new employee(id, name, depId);
					flag = 0;
					break;
				case 2:
					work = new manager(id, name, depId);
					flag = 0;
					break;
				case 3:
					work = new boss(id, name, depId);
					flag = 0;
					break;
				default:
					cout << "输入有误,请重新输入" << endl;
					break;
				}
			}
			//保存新数据,更新有效信息数
			if (this->workStart == nullptr)
			{
				cout << "this->workStart == nullptr" << endl;
				exit(-1);
			}
			this->workStart[this->curSize] = work;

			this->curSize++;
			//更新文件状态
			this->isEmpty = false;
			cout << "添加成功" << endl;
		}
		cout << "添加" << addNum << "名员工成功" << endl;
		//自动保存文件
		this->saveFile();
	}
	else
	{
		cout << "输入错误" << endl;
		return;
	}

	//按任意键清屏后，回到菜单
	system("pause");
	system("cls");
}