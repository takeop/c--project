#pragma once
#include <iostream>
#include <string>
#include "work.h"
using namespace std;


class employee : public work
{
public:
	//初始化员工信息
	employee(int id, string name, int depId);
	//显示员工信息
	virtual void showInfo();
	//获取岗位名称
	virtual string getDepName();
	//岗位职责
	virtual string showDuty();
};
