#define _CRT_SECURE_NO_WARNINGS 1

#include "workerManager.h"
#include "employee.h"
#include "manager.h"
#include "boss.h"
void test01()
{
	/*work* wok = nullptr;
	wok = new employee(1, "张三", 1);
	wok->showInfo();
	delete wok;


	wok = new manager(2, "老王", 2);
	wok->showInfo();
	delete wok;

	wok = new boss(3, "王久", 3);
	wok->showInfo();
	delete wok;*/
	workerManager wm;  //实例化对象
	int choice = 0;   //存储用户选择
	while (true)
	{
		wm.showMenu();
		cout << "请输入调用功能" << endl;
		cin >> choice;
		switch (choice)
		{
		case 0: //退出系统
			wm.exitSystem();
			break;
		case 1:  //增加职工
			wm.addEmployee();
			break;
		case 2:  //显示职工
			wm.showEmployee();
			break;
		case 3:  //删除离职职工
		{
			wm.delEmployee();
		}
			break;
		case 4:  //修改职工信息
			wm.modefyEmployee();
			break;
		case 5:  //查找职工信息
			wm.searchEmployee();
			break;
		case 6:  //按编号排序职工
			wm.sortEmployee();
			break;
		case 7: //清空所有文档
			wm.cleanFile();
			break;
		default:
			system("cls");
			break;
		}
	}
	
}

int main()
{
	test01();
	

	return 0;
}